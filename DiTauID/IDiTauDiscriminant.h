// Dear emacs, this is -*- c++ -*-
#ifndef DITAUID_DITAUDISCRIMINANT_H
#define DITAUID_DITAUDISCRIMINANT_H

/*
  author: David Kirchmeier
  mail: david.kirchmeier@cern.ch
*/

// Framework include(s):
#include "AsgTools/IAsgTool.h"

// EDM include(s):
#include "xAODTau/TauJet.h"
#include "xAODTau/DiTauJetContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticleAuxContainer.h"


namespace DiTauID
{

class IDiTauDiscriminant :
  public virtual asg::IAsgTool
{

  /// Declare the interface that the class provides
  ASG_TOOL_INTERFACE( DiTauID::IDiTauDiscriminant )

public:
  // initialize the tool
  virtual StatusCode initialize() = 0;

  // set pointer to event
  virtual StatusCode initializeEvent() = 0;

  // calculate ID variables
  virtual double getJetBDTScore(const xAOD::DiTauJet& xDiTau) = 0;

}; // class IDiTauDiscriminant

} // namespace DiTauID

#endif // DITAUID_DITAUDISCRIMINANT_H


