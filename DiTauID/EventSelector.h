#ifndef DITAUID_EVENTSELECTOR_H
#define DITAUID_EVENTSELECTOR_H

#include <EventLoop/Algorithm.h>
#include "xAODEventInfo/EventInfo.h"

#include <string>

// tools
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
// #include "TriggerMatchingTool/IMatchingTool.h"
#include "TriggerMatchingTool/MatchingTool.h"
#include "JetSelectorTools/JetCleaningTool.h"

class EventSelector : public EL::Algorithm
{
public:
  EventSelector();
  EventSelector (std::string const&);
  ~EventSelector ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode initialize();
  bool checkGRL(xAOD::TEvent* event);
  bool checkTrigger(xAOD::TEvent* event);
  bool checkJetCleaning(xAOD::TEvent* event);
  bool checkEvent(xAOD::TEvent* event);

  // virtual EL::StatusCode fileExecute ();
  // virtual EL::StatusCode histInitialize ();
  // virtual EL::StatusCode changeInput (bool firstFile);
  // virtual EL::StatusCode initialize ();
  // virtual EL::StatusCode execute ();
  // virtual EL::StatusCode postExecute ();
  // virtual EL::StatusCode finalize ();
  // virtual EL::StatusCode histFinalize ();


private:
  GoodRunsListSelectionTool *m_grlTool; //!
  Trig::TrigDecisionTool *m_trigDecisionTool; //!
  TrigConf::xAODConfigTool *m_trigConfigTool; //!
  // Trig::MatchingTool* m_tmt; //!
  JetCleaningTool *m_jetCleaning; //!


  std::string m_name; //!
  // bool isMC; //!

  // bool checkCuts (const xAOD::DiTauJet* xDiTau);
  // void fillTree (const xAOD::DiTauJet* xDiTau);
  
public:
  // this is needed to distribute the algorithm to the workers
  ClassDef(EventSelector, 1);
};

#endif
