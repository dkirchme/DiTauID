#ifndef DiTauID_DTNTmaker_H
#define DiTauID_DTNTmaker_H

#include <EventLoop/Algorithm.h>

// EDM includes
#include "xAODTau/DiTauJetContainer.h"

// ROOT includes
#include <TTree.h>

// Tools
#include "DiTauID/EventSelector.h"


class DTNTmaker : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // defining the output file for ntuple output
  // the ntuple will contain the variables for training
  std::string outputName;
  TTree *tree; //!
  // branches in the DTNT
  int isSignal; //!
  int EventNumber; //!
  bool isMC; //!
  double ev_weight; //!
  double sample_weight; //!
  double mu; //!
  int n_vtx; //!
  double jet_pt; //!

  double eta; //!
  double phi; //!
  double m; //!
  double pt_lead; //!
  double pt_subl; //!
  double eta_lead; //!
  double eta_subl; //!
  double phi_lead; //!
  double phi_subl; //!
  double e_lead; //!
  double e_subl; //!

  double ditau_pt; //!
  
  double f_core_lead; //!
  double f_core_subl; //!
  
  double f_subjet_lead; //!
  double f_subjet_subl; //!
  double f_subjets; //!
  
  double E_frac_subl; //!
  double E_frac_subsubl; //!
  
  double f_track_lead; //!
  double f_track_subl; //!
  double f_isotracks; //!
  double f_clusters; //!
  
  double R_max_lead; //!
  double R_max_subl; //!
  
  double R_track; //!
  double R_track_all; //!
  double R_track_core; //!
  double R_isotrack; //!
  
  double R_core_lead; //!
  double R_core_subl; //!
  double R_tracks_lead; //!
  double R_tracks_subl; //!
  
  double R_subjets_subl; //!
  double R_subjets_subsubl; //!
  
  double m_track; //!
  double m_track_all; //!
  double m_track_core; //!
  
  double m_core_lead; //!
  double m_core_subl; //!
  double m_tracks_lead; //!
  double m_tracks_subl; //!
  
  double d0_leadtrack_lead; //!
  double d0_leadtrack_subl; //!
  
  int n_track; //!
  int n_tracks_lead; //!
  int n_tracks_subl; //!
  int n_isotrack; //!
  int n_othertrack; //!
  int n_iso_ellipse; //!
  int n_Subjets; //!

  int n_antikt_subjets; //!
  int n_ca_subjets; //!
  double mu_massdrop; //!
  double y_massdrop; //!
  
  double JetBDT; //!

  double trigger_pt; //!
  double trigger_prescale; //!

  char truth_matched; //!

  double truth_pt_vis_lead; //!
  double truth_eta_vis_lead; //!
  double truth_phi_vis_lead; //!
  double truth_E_vis_lead; //!
  double truth_m_vis_lead; //!
  double truth_n_tracks_lead; //!

  double truth_pt_vis_subl; //!
  double truth_eta_vis_subl; //!
  double truth_phi_vis_subl; //!
  double truth_E_vis_subl; //!
  double truth_m_vis_subl; //!
  double truth_n_tracks_subl; //!


  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!



  // this is a standard constructor
  DTNTmaker ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(DTNTmaker, 1);

private:
  bool checkCuts (const xAOD::DiTauJet* xDiTau);
  void fillTree (const xAOD::DiTauJet* xDiTau);

  EventSelector* m_evtSelector; //!
};

#endif
