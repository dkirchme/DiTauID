// Dear emacs, this is -*- c++ -*-
#ifndef DITAUID_IWEIGHTSCALCULATOR_H
#define DITAUID_IWEIGHTSCALCULATOR_H

/*
  author: David Kirchmeier
  mail: david.kirchmeier@cern.ch
*/

// Framework include(s):
#include "AsgTools/IAsgTool.h"

// EDM include(s):
#include "xAODTau/TauJet.h"
#include "xAODTau/DiTauJetContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticleAuxContainer.h"


namespace DiTauID
{

class IWeightsCalculator :
  public virtual asg::IAsgTool
{

  /// Declare the interface that the class provides
  ASG_TOOL_INTERFACE( DiTauID::IWeightsCalculator )

public:
  // initialize the tool
  virtual StatusCode initialize() = 0;

  // set pointer to event
  virtual StatusCode initializeEvent() = 0;

  // calculate pt weights
  virtual StatusCode calculatePtWeights() = 0;



}; // class IWeightsCalculator

} // namespace DiTauID

#endif // DITAUID_IWEIGHTSCALCULATOR_H


