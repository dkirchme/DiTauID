// Dear emacs, this is -*- c++ -*-
#ifndef  DITAUID_WEIGHTSCALCULATOR_H
#define  DITAUID_WEIGHTSCALCULATOR_H

/*
  author: David Kirchmeier
  mail: david.kirchmeier@cern.ch
*/

// Framework includes
#include "AsgTools/AsgTool.h"

// Local includes
#include "DiTauID/IWeightsCalculator.h"

// Root includes
#include "TTree.h"
#include "TFile.h"
#include "TChain.h" 

#include "TH1F.h"

#pragma GCC push_options
#pragma GCC optimize ("O0")

namespace DiTauID
{


class WeightsCalculator
  : public DiTauID::IWeightsCalculator
  , public asg::AsgTool
{
  /// Create a proper constructor for Athena
  ASG_TOOL_CLASS( WeightsCalculator,
                  DiTauID::IWeightsCalculator )

public:

  WeightsCalculator( const std::string& name );

  virtual ~WeightsCalculator();

  // initialize the tool
  virtual StatusCode initialize();

  // set pointer to event
  virtual StatusCode initializeEvent();

  // calculate weights
  virtual StatusCode calculateWeights();

private:
  
  virtual StatusCode calculatePtWeights();
  virtual StatusCode calculatePileupWeights();
  
  TFile *m_fSig;
  TFile *m_fBkg;
  TFile *m_fOutSig;
  TFile *m_fOutBkg;

  TTree *m_treeSig;
  TTree *m_treeBkg;

  // steering variables
  std::string m_sInputFolderSig;
  std::string m_sInputFolderBkg;
  std::string m_sInputFilePatternSig;
  std::string m_sInputFilePatternBkg;
  std::string m_sTreeNameSig;
  std::string m_sTreeNameBkg;
  std::string m_sOutputFileSig;
  std::string m_sOutputFileBkg;
  bool m_bFlatPtWeight;


}; // class WeightsCalculator

}

#pragma GCC pop_options

#endif // DITAUID_WEIGHTSCALCULATOR_H
