// #include <TauAnalysisTools/TauTruthMatchingTool.h>
#include <DiTauID/DiTauDiscriminant.h>

// Core include(s):
#include "AthLinks/ElementLink.h"

// EDM include(s):
#include "xAODTau/DiTauJet.h"
#include "xAODTau/DiTauJetContainer.h"

// ROOT includes
#include "TMVA/Reader.h"

// boost
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>


// BDT weights
// #include "DiTauID/TMVAClassificationBDT.cxx"

using namespace DiTauID;


/// Helper macros for checking and retrieving xAOD containers
#define CHECK( ARG )                    \
  do {                                                  \
    const bool result = ARG;                \
    if( ! result ) {                    \
      ::Error( "calcDiTauVariables", "Failed to execute: \"%s\"",  \
           #ARG );                  \
      return 1;                     \
    }                           \
  } while( false )

#define RETRIEVE( TYPE, CONTAINER , NAME )              \
  do {                                  \
  if (evtStore()->contains<TYPE>(NAME))                  \
    CHECK( evtStore()->retrieve( CONTAINER, NAME ) );            \
  else                                  \
    Warning("calcDiTauVariables","%s container is not available", NAME);       \
  } while(false)                            \
 

//=================================PUBLIC-PART==================================
//______________________________________________________________________________
DiTauDiscriminant::DiTauDiscriminant( const std::string& name )
  : AsgTool(name)
  , m_rReader(0)
  , m_bIsInitialized(false)
{
  declareProperty( "WeightsFile", m_sWeightsFile = "");
}

//______________________________________________________________________________
DiTauDiscriminant::~DiTauDiscriminant( )
{

}

//______________________________________________________________________________
StatusCode DiTauDiscriminant::initialize()
{  
   // if (!m_bIsInitialized)
   //    return StatusCode::SUCCESS;

   ATH_MSG_INFO( "Initializing DiTauDiscriminant" );
   ATH_MSG_DEBUG( "path to weights file: " << m_sWeightsFile );

   m_rReader = new TMVA::Reader( "Silent" );

   m_mIDSpectators = {
      {"ditau_pt", 0},
      {"mu", 0},
      {"pt_weight", 0},
      {"isSignal", 0}
   };

   m_mIDVariables = {
      {"f_core_lead", 0},
      {"f_core_subl", 0},
      {"f_subjet_lead", 0},
      {"f_subjet_subl", 0},
      {"f_subjets", 0},
      {"f_track_lead", 0},
      {"f_track_subl", 0},
      {"R_max_lead", 0},
      {"R_max_subl", 0},
      {"n_Subjets", 0},
      {"n_track", 0},
      {"n_tracks_lead", 0},
      {"n_tracks_subl", 0},
      {"n_isotrack", 0},
      {"n_othertrack", 0},
      {"R_track", 0},
      {"R_track_core", 0},
      {"R_track_all", 0},
      {"R_isotrack", 0},
      {"R_core_lead", 0},
      {"R_core_subl", 0},
      {"R_tracks_lead", 0},
      {"R_tracks_subl", 0},
      {"m_track", 0},
      {"m_track_core", 0},
      {"m_core_lead", 0},
      {"m_core_subl", 0},
      {"m_track_all", 0},
      {"m_tracks_lead", 0},
      {"log(m_tracks_lead)", 0},
      {"m_tracks_subl", 0},
      {"log(m_tracks_subl)", 0},
      {"E_frac_subl", 0},
      {"E_frac_subsubl", 0},
      {"R_subjets_subl", 0},
      {"R_subjets_subsubl", 0},
      {"d0_leadtrack_lead", 0},
      {"log(abs(d0_leadtrack_lead))", 0},
      {"d0_leadtrack_subl", 0},
      {"log(abs(d0_leadtrack_subl))", 0},
      {"f_isotracks", 0},
      {"log(f_isotracks)", 0},
      {"n_iso_ellipse", 0},
      {"n_antikt_subjets", 0},
      {"n_ca_subjets", 0},
      {"mu_massdrop", 0},
      {"y_massdrop", 0}
   };

   parseWeightsFile();

   m_bIsInitialized = true;
   return StatusCode::SUCCESS;
}

//______________________________________________________________________________
StatusCode DiTauDiscriminant::initializeEvent()
{
  return StatusCode::SUCCESS;
}


////////////////////////////////////////////////////////////////////////////////
//                              Wrapper functions                             //
////////////////////////////////////////////////////////////////////////////////
double DiTauDiscriminant::getJetBDTScore(const xAOD::DiTauJet& xDiTau)
{
  setIDVariables(xDiTau);

  double bdtScore = m_rReader->EvaluateMVA("JetBDT");

  xDiTau.auxdecor<double>("JetBDT") = bdtScore;

  ATH_MSG_DEBUG("Jet BDT score: " << bdtScore);
  ATH_MSG_DEBUG("===========================================================");
  ATH_MSG_DEBUG("===========================================================");
  return bdtScore;
}



//=================================PRIVATE-PART=================================
//______________________________________________________________________________

void DiTauDiscriminant::parseWeightsFile()
{
   boost::property_tree::ptree tree;
   boost::property_tree::read_xml(m_sWeightsFile, tree);

   for(auto& v: tree.get_child("MethodSetup.Variables"))
   {
      if (v.first == "<xmlattr>")
         continue;

      boost::property_tree::ptree subtree = v.second;
      std::string varname = subtree.get<std::string>("<xmlattr>.Expression", "");
      if (varname == "")
      {
         ATH_MSG_WARNING("Found empty variable name. Continue.");
         continue;
      }
      ATH_MSG_DEBUG("Found variable: " << varname);

      if (m_mIDVariables.find(varname) == m_mIDVariables.end())
      {
        ATH_MSG_WARNING("Could not find ID variable " << varname << " in predefined list of ID variables.");
      }
      m_rReader->AddVariable(varname, &m_mIDVariables[varname]);
      m_vVarNames.push_back(varname);
   }

   for (auto& v: tree.get_child("MethodSetup.Spectators"))
   {
      if (v.first == "<xmlattr>")
         continue;

      boost::property_tree::ptree subtree = v.second;
      std::string specname = subtree.get<std::string>("<xmlattr>.Expression", "");
      if (specname == "")
      {
         ATH_MSG_WARNING("Found empty spectator name. Continue.");
         continue;
      }
      ATH_MSG_DEBUG("Found spectator: " << specname);
      m_rReader->AddSpectator(specname, &m_mIDSpectators[specname]);
   }

   m_rReader->BookMVA("JetBDT", m_sWeightsFile.c_str());
}

// ----------------------------------------------------------------------------
void DiTauDiscriminant::setIDVariables(const xAOD::DiTauJet& xDiTau)
{
  m_mIDVariables["f_core_lead"] = (float) xDiTau.auxdata<double>("f_core_lead");
  ATH_MSG_DEBUG("f_core_lead: " << m_mIDVariables["f_core_lead"]);
  m_mIDVariables["f_core_subl"] = (float) xDiTau.auxdata<double>("f_core_subl");
  ATH_MSG_DEBUG("f_core_subl: " << m_mIDVariables["f_core_subl"]);
  m_mIDVariables["f_subjet_lead"] = (float) xDiTau.auxdata<double>("f_subjet_lead");
  ATH_MSG_DEBUG("f_subjet_lead: " << m_mIDVariables["f_subjet_lead"]);
  m_mIDVariables["f_subjet_subl"] = (float) xDiTau.auxdata<double>("f_subjet_subl");
  ATH_MSG_DEBUG("f_subjet_subl: " << m_mIDVariables["f_subjet_subl"]);
  m_mIDVariables["f_subjets"] = (float) xDiTau.auxdata<double>("f_subjets");
  ATH_MSG_DEBUG("f_subjets: " << m_mIDVariables["f_subjets"]);
  m_mIDVariables["f_track_lead"] = (float) xDiTau.auxdata<double>("f_track_lead");
  ATH_MSG_DEBUG("f_track_lead: " << m_mIDVariables["f_track_lead"]);
  m_mIDVariables["f_track_subl"] = (float) xDiTau.auxdata<double>("f_track_subl");
  ATH_MSG_DEBUG("f_track_subl: " << m_mIDVariables["f_track_subl"]);
  m_mIDVariables["R_max_lead"] = (float) xDiTau.auxdata<double>("R_max_lead");
  ATH_MSG_DEBUG("R_max_lead: " << m_mIDVariables["R_max_lead"]);
  m_mIDVariables["R_max_subl"] = (float) xDiTau.auxdata<double>("R_max_subl");
  ATH_MSG_DEBUG("R_max_subl: " << m_mIDVariables["R_max_subl"]);
  m_mIDVariables["n_Subjets"] = (float) xDiTau.nSubjets();
  ATH_MSG_DEBUG("n_Subjets: " << m_mIDVariables["n_Subjets"]);
  m_mIDVariables["n_track"] = (float) xDiTau.auxdata<int>("n_track");
  ATH_MSG_DEBUG("n_track: " << m_mIDVariables["n_track"]);
  m_mIDVariables["n_tracks_lead"] = (float) xDiTau.auxdata<int>("n_tracks_lead");
  ATH_MSG_DEBUG("n_tracks_lead: " << m_mIDVariables["n_tracks_lead"]);
  m_mIDVariables["n_tracks_subl"] = (float) xDiTau.auxdata<int>("n_tracks_subl");
  ATH_MSG_DEBUG("n_tracks_subl: " << m_mIDVariables["n_tracks_subl"]);
  m_mIDVariables["n_isotrack"] = (float) xDiTau.auxdata<int>("n_isotrack");
  ATH_MSG_DEBUG("n_isotrack: " << m_mIDVariables["n_isotrack"]);
  m_mIDVariables["n_othertrack"] = (float) xDiTau.auxdata<int>("n_othertrack");
  ATH_MSG_DEBUG("n_othertrack: " << m_mIDVariables["n_othertrack"]);
  m_mIDVariables["R_track"] = (float) xDiTau.auxdata<double>("R_track");
  ATH_MSG_DEBUG("R_track: " << m_mIDVariables["R_track"]);
  m_mIDVariables["R_track_core"] = (float) xDiTau.auxdata<double>("R_track_core");
  ATH_MSG_DEBUG("R_track_core: " << m_mIDVariables["R_track_core"]);
  m_mIDVariables["R_track_all"] = (float) xDiTau.auxdata<double>("R_track_all");
  ATH_MSG_DEBUG("R_track_all: " << m_mIDVariables["R_track_all"]);
  m_mIDVariables["R_isotrack"] = (float) xDiTau.auxdata<double>("R_isotrack");
  ATH_MSG_DEBUG("R_isotrack: " << m_mIDVariables["R_isotrack"]);
  m_mIDVariables["R_core_lead"] = (float) xDiTau.auxdata<double>("R_core_lead");
  ATH_MSG_DEBUG("R_core_lead: " << m_mIDVariables["R_core_lead"]);
  m_mIDVariables["R_core_subl"] = (float) xDiTau.auxdata<double>("R_core_subl");
  ATH_MSG_DEBUG("R_core_subl: " << m_mIDVariables["R_core_subl"]);
  m_mIDVariables["R_tracks_lead"] = (float) xDiTau.auxdata<double>("R_tracks_lead");
  ATH_MSG_DEBUG("R_tracks_lead: " << m_mIDVariables["R_tracks_lead"]);
  m_mIDVariables["R_tracks_subl"] = (float) xDiTau.auxdata<double>("R_tracks_subl");
  ATH_MSG_DEBUG("R_tracks_subl: " << m_mIDVariables["R_tracks_subl"]);
  m_mIDVariables["m_track"] = (float) xDiTau.auxdata<double>("m_track");
  ATH_MSG_DEBUG("m_track: " << m_mIDVariables["m_track"]);
  m_mIDVariables["m_track_core"] = (float) xDiTau.auxdata<double>("m_track_core");
  ATH_MSG_DEBUG("m_track_core: " << m_mIDVariables["m_track_core"]);
  m_mIDVariables["m_core_lead"] = (float) xDiTau.auxdata<double>("m_core_lead");
  ATH_MSG_DEBUG("m_core_lead: " << m_mIDVariables["m_core_lead"]);
  m_mIDVariables["m_core_subl"] = (float) xDiTau.auxdata<double>("m_core_subl");
  ATH_MSG_DEBUG("m_core_subl: " << m_mIDVariables["m_core_subl"]);
  m_mIDVariables["m_track_all"] = (float) xDiTau.auxdata<double>("m_track_all");
  ATH_MSG_DEBUG("m_track_all: " << m_mIDVariables["m_track_all"]);
  m_mIDVariables["m_tracks_lead"] = (float) xDiTau.auxdata<double>("m_tracks_lead");
  ATH_MSG_DEBUG("m_tracks_lead: " << m_mIDVariables["m_tracks_lead"]);
  m_mIDVariables["log(m_tracks_lead)"] = log(m_mIDVariables["m_tracks_lead"]);
  ATH_MSG_DEBUG("log(m_tracks_lead): " << m_mIDVariables["log(m_tracks_lead)"]);
  m_mIDVariables["m_tracks_subl"] = (float) xDiTau.auxdata<double>("m_tracks_subl");
  ATH_MSG_DEBUG("m_tracks_subl: " << m_mIDVariables["m_tracks_subl"]);
  m_mIDVariables["log(m_tracks_subl)"] = log(m_mIDVariables["m_tracks_subl"]);
  ATH_MSG_DEBUG("log(m_tracks_subl): " << m_mIDVariables["log(m_tracks_subl)"]);
  m_mIDVariables["E_frac_subl"] = (float) xDiTau.auxdata<double>("E_frac_subl");
  ATH_MSG_DEBUG("E_frac_subl: " << m_mIDVariables["E_frac_subl"]);
  m_mIDVariables["E_frac_subsubl"] = (float) xDiTau.auxdata<double>("E_frac_subsubl");
  ATH_MSG_DEBUG("E_frac_subsubl: " << m_mIDVariables["E_frac_subsubl"]);
  m_mIDVariables["R_subjets_subl"] = (float) xDiTau.auxdata<double>("R_subjets_subl");
  ATH_MSG_DEBUG("R_subjets_subl: " << m_mIDVariables["R_subjets_subl"]);
  m_mIDVariables["R_subjets_subsubl"] = (float) xDiTau.auxdata<double>("R_subjets_subsubl");
  ATH_MSG_DEBUG("R_subjets_subsubl: " << m_mIDVariables["R_subjets_subsubl"]);
  m_mIDVariables["d0_leadtrack_lead"] = (float) xDiTau.auxdata<double>("d0_leadtrack_lead");
  ATH_MSG_DEBUG("d0_leadtrack_lead: " << m_mIDVariables["d0_leadtrack_lead"]);
  m_mIDVariables["abs(d0_leadtrack_lead)"] = fabs(m_mIDVariables["d0_leadtrack_lead"]);
  ATH_MSG_DEBUG("abs(d0_leadtrack_lead): " << m_mIDVariables["abs(d0_leadtrack_lead)"]);
  m_mIDVariables["log(abs(d0_leadtrack_lead))"] = log(fabs(m_mIDVariables["d0_leadtrack_lead"]));
  ATH_MSG_DEBUG("log(abs(d0_leadtrack_lead)): " << m_mIDVariables["log(abs(d0_leadtrack_lead))"]);
  m_mIDVariables["d0_leadtrack_subl"] = (float) xDiTau.auxdata<double>("d0_leadtrack_subl");
  ATH_MSG_DEBUG("d0_leadtrack_subl: " << m_mIDVariables["d0_leadtrack_subl"]);
  m_mIDVariables["abs(d0_leadtrack_subl)"] = fabs(m_mIDVariables["d0_leadtrack_subl"]);
  ATH_MSG_DEBUG("abs(d0_leadtrack_subl): " << m_mIDVariables["abs(d0_leadtrack_subl)"]);
  m_mIDVariables["log(abs(d0_leadtrack_subl))"] = log(fabs(m_mIDVariables["d0_leadtrack_subl"]));
  ATH_MSG_DEBUG("log(abs(d0_leadtrack_subl)): " << m_mIDVariables["log(abs(d0_leadtrack_subl))"]);
  m_mIDVariables["f_isotracks"] = (float) xDiTau.auxdata<double>("f_isotracks");
  ATH_MSG_DEBUG("f_isotracks: " << m_mIDVariables["f_isotracks"]);
  m_mIDVariables["log(f_isotracks)"] = log(m_mIDVariables["f_isotracks"]);
  ATH_MSG_DEBUG("log(f_isotracks): " << m_mIDVariables["log(f_isotracks)"]);
  m_mIDVariables["n_iso_ellipse"] = (float) xDiTau.auxdata<int>("n_iso_ellipse");
  ATH_MSG_DEBUG("n_iso_ellipse: " << m_mIDVariables["n_iso_ellipse"]);
  m_mIDVariables["n_antikt_subjets"] = (float) xDiTau.auxdata<int>("n_antikt_subjets");
  ATH_MSG_DEBUG("n_antikt_subjets: " << m_mIDVariables["n_antikt_subjets"]);
  m_mIDVariables["n_ca_subjets"] = (float) xDiTau.auxdata<int>("n_ca_subjets");
  ATH_MSG_DEBUG("n_ca_subjets: " << m_mIDVariables["n_ca_subjets"]);
  m_mIDVariables["mu_massdrop"] = (float) xDiTau.auxdata<double>("mu_massdrop");
  ATH_MSG_DEBUG("mu_massdrop: " << m_mIDVariables["mu_massdrop"]);
  m_mIDVariables["y_massdrop"] = (float) xDiTau.auxdata<double>("y_massdrop");
  ATH_MSG_DEBUG("y_massdrop: " << m_mIDVariables["y_massdrop"]);

  ATH_MSG_DEBUG("-------------------------------------------");
  for (const auto var: m_vVarNames)
  {
    ATH_MSG_DEBUG(var << ": " << m_mIDVariables[var]);

  }
}
