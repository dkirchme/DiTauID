#include <DiTauID/DTNTmaker.h>
#include <DiTauID/DiTauIDVarCalculator.h>
#include <DiTauID/DiTauDiscriminant.h>
#include <DiTauID/EventSelector.h>

#ifdef __ROOTCLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ class DiTauIDVarCalculator+;
#pragma link C++ class DTNTmaker+;
#pragma link C++ class EventSelector+;
#pragma link C++ class DiTauDiscriminant+;

#endif

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ class DiTauIDVarCalculator+;
#pragma link C++ class DTNTmaker+;
#pragma link C++ class EventSelector+;
#pragma link C++ class DiTauDiscriminant+;

#endif