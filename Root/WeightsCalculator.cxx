#include <DiTauID/WeightsCalculator.h>

#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/Sample.h"
// #include "SampleHandler/ToolsDiscovery.h"
// #include "SampleHandler/DiskListLocal.h"

#include <AsgTools/MessageCheck.h>



// Root includes
#include "TH1F.h"
#include "TBranch.h"
#include "TF1.h"

using namespace DiTauID;

#pragma GCC push_options
#pragma GCC optimize ("O0")


//=================================PUBLIC-PART==================================
//______________________________________________________________________________
WeightsCalculator::WeightsCalculator( const std::string& name )
  : AsgTool(name)
  , m_sInputFolderSig("")
  , m_sInputFolderBkg("")
  , m_sInputFilePatternSig("")
  , m_sInputFilePatternBkg("")
  , m_sTreeNameSig("")
  , m_sTreeNameBkg("")
  , m_sOutputFileSig("")
  , m_sOutputFileBkg("")
{
  declareProperty( "InputFolderSignal", m_sInputFolderSig = "" );
  declareProperty( "InputFolderBackground", m_sInputFolderBkg = "" );
  declareProperty( "InputFilePatternSignal", m_sInputFilePatternSig = "" );
  declareProperty( "InputFilePatternBackground", m_sInputFilePatternBkg = "" );
  declareProperty( "TreeNameSignal", m_sTreeNameSig = "tree" );
  declareProperty( "TreeNameBackground", m_sTreeNameBkg = "tree" );
  declareProperty( "OutputFileSignal", m_sOutputFileSig = "DTNT_Signal.root" );
  declareProperty( "OutputFileBackground", m_sOutputFileBkg = "DTNT_Background.root" );
  declareProperty( "FlatPtWeight", m_bFlatPtWeight = false);
}

//______________________________________________________________________________
WeightsCalculator::~WeightsCalculator( )
{

}

//______________________________________________________________________________
StatusCode WeightsCalculator::initialize()
{
  ATH_MSG_INFO( "Initializing WeightsCalculator" );
  
  // ----------------------------------------------------------------------------
  // get input files
  // ----------------------------------------------------------------------------
  // shSig is a sample handler that handles signal samples
  SH::SampleHandler shSig;
  SH::ScanDir()
    .filePattern(m_sInputFilePatternSig)
    .scan(shSig, m_sInputFolderSig);
  shSig.setMetaString("nc_tree", m_sTreeNameSig);

  // shBkg is a sample handler that handles background samples
  SH::SampleHandler shBkg;
  SH::ScanDir()
    .filePattern(m_sInputFilePatternBkg)
    .scan(shBkg, m_sInputFolderBkg);
  shBkg.setMetaString("nc_tree", m_sTreeNameBkg);

  ATH_MSG_INFO("background samples:");
  shBkg.print();
  ATH_MSG_INFO("signal samples");
  shSig.print();

  TChain* chSig  = shSig.at(0)->makeTChain();
  TChain* chBkg = new TChain();
  for (const auto sample: shBkg)
  {
    chBkg->Add(sample->makeTChain());
  }

  // ATH_MSG_INFO("signal chain:");
  // chSig->Print();
  // ATH_MSG_INFO("background chain:");
  // chBkg->Print();

  // --------------------------------------------------------------------
  // get trees from input files
  // --------------------------------------------------------------------
  m_fOutSig = new TFile(m_sOutputFileSig.c_str(), "RECREATE");
  m_treeSig = chSig->CloneTree(chSig->GetEntries());

  m_fOutBkg = new TFile(m_sOutputFileBkg.c_str(), "RECREATE");
  m_treeBkg = chBkg->CloneTree(chBkg->GetEntries());

  if (m_treeSig == nullptr)
  {
    ATH_MSG_ERROR("Could not find signal tree with name " << m_sOutputFileSig << " in input files.");
    return StatusCode::FAILURE;
  }
  if (m_treeBkg == nullptr)
  {
    ATH_MSG_ERROR("Could not find background tree with name " << m_sOutputFileBkg << " in input files.");
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

//______________________________________________________________________________
StatusCode WeightsCalculator::initializeEvent()
{
  // RETRIEVE(xAOD::DiTauJetContainer, m_xDiTauContainer, m_sDiTauContainerName);
  return StatusCode::SUCCESS;
}


////////////////////////////////////////////////////////////////////////////////
//                              Wrapper functions                             //
////////////////////////////////////////////////////////////////////////////////
StatusCode WeightsCalculator::calculateWeights()
{
  ANA_CHECK( calculatePileupWeights() );
  ANA_CHECK( calculatePtWeights() );

  m_fOutSig->Write();
  m_fOutBkg->Write();

  return StatusCode::SUCCESS;
}

////////////////////////////////////////////////////////////////////////////////
//                              Private functions                             //
////////////////////////////////////////////////////////////////////////////////
StatusCode WeightsCalculator::calculatePtWeights()
{
  ATH_MSG_DEBUG("Calculate pt weights");

  TH1F* h_pt_sig = new TH1F("h_pt_sig", "h_pt_sig", 30, 0, 2000*1000);
  TH1F* h_pt_bkg = new TH1F("h_pt_bkg", "h_pt_bkg", 30, 0, 2000*1000);

  // m_treeSig->Print();
  m_treeSig->Project("h_pt_sig", "ditau_pt", "", "NORM");
  m_treeBkg->Project("h_pt_bkg", "ditau_pt", "", "NORM");

  TH1F* h_w_pt_bkg = new TH1F("h_w_pt_bkg", "h_w_pt_bkg", 30, 0, 2000*1000);
  TH1F* h_w_pt_sig = new TH1F("h_w_pt_sig", "h_w_pt_sig", 30, 0, 2000*1000);
  if (m_bFlatPtWeight)
  {
    TF1* f_flat = new TF1("flat", "0.5*( 1+erf( (-x+1200*1000)/(sqrt(2)*1200*100) ) )", 0, 2000*1000);

    h_w_pt_bkg->Divide(h_pt_bkg, h_pt_bkg);
    h_w_pt_bkg->Multiply(f_flat);
    h_w_pt_bkg->Scale(1./h_w_pt_bkg->Integral());
    h_w_pt_bkg->Divide(h_pt_bkg);

    h_w_pt_sig->Divide(h_pt_sig, h_pt_sig);
    h_w_pt_sig->Multiply(f_flat);
    h_w_pt_sig->Scale(1./h_w_pt_sig->Integral());
    h_w_pt_sig->Divide(h_pt_sig);
  }
  else
  {
    h_w_pt_bkg->Divide(h_pt_sig, h_pt_bkg);
    h_w_pt_sig->Divide(h_pt_sig, h_pt_sig);
  }


  // ----------------------------------------------------------------------------
  // fill background tree with pt weights
  // ----------------------------------------------------------------------------
  double w_pt_bkg;
  TBranch *b_pt_bkg = m_treeBkg->Branch("pt_weight", &w_pt_bkg, "pt_weight/D");

  double pt_bkg = 0;
  m_treeBkg->SetBranchAddress("ditau_pt", &pt_bkg);

  int n_bkg = m_treeBkg->GetEntries();
  // ATH_MSG_INFO("n_bkg: " << n_bkg);
  for (int i = 0; i < n_bkg; i++) 
  {
     m_treeBkg->GetEntry(i);
     // ATH_MSG_INFO("pt bkg: " << pt_bkg);
     if (pt_bkg < 2000*1000)
     {
        int bin = h_w_pt_bkg->FindBin(pt_bkg);
        w_pt_bkg = h_w_pt_bkg->GetBinContent(bin);
     }
     else
     {
        w_pt_bkg = 0.;
     }

     b_pt_bkg->Fill();
  }

  // ----------------------------------------------------------------------------
  // fill signal tree with pt weights  (= 1)
  // ----------------------------------------------------------------------------
  double w_pt_sig;
  TBranch *b_pt_sig = m_treeSig->Branch("pt_weight", &w_pt_sig, "pt_weight/D");

  double pt_sig = 0;
  m_treeSig->SetBranchAddress("ditau_pt", &pt_sig);

  int n_sig = m_treeSig->GetEntries();
  // ATH_MSG_INFO("n_sig: " << n_sig);
  // for (int i = 0; i < n_sig; i++) 
  for (int i = 0; i < m_treeSig->GetEntries(); i++) 
  { 
     m_treeSig->GetEntry(i);
     // ATH_MSG_INFO("pt sig: " << pt_sig);
     if (pt_sig < 2000*1000)
     {
        int bin = h_w_pt_sig->FindBin(pt_sig);
        w_pt_sig = h_w_pt_sig->GetBinContent(bin);      
     }
     else
     {
        w_pt_sig = 0;
     }
     b_pt_sig->Fill();
  }

  return StatusCode::SUCCESS;
}

// ----------------------------------------------------------------------------
StatusCode WeightsCalculator::calculatePileupWeights()
{
  ATH_MSG_DEBUG("Calculate pileup weights");

  TH1F* h_mu_sig = new TH1F("h_mu_sig", "h_mu_sig", 50, 0, 50);
  TH1F* h_mu_bkg = new TH1F("h_mu_bkg", "h_mu_bkg", 50, 0, 50);

  m_treeSig->Project("h_mu_sig", "mu", "", "NORM");
  m_treeBkg->Project("h_mu_bkg", "mu", "", "NORM");

  TH1F* h_w_mu = new TH1F("h_w_mu", "h_w_mu", 50, 0, 50);
  h_w_mu->Divide(h_mu_bkg, h_mu_sig);


  // ----------------------------------------------------------------------------
  // fill background tree with mu weights (==1)
  // ----------------------------------------------------------------------------
  double w_mu_bkg = 1;
  TBranch *b_mu_bkg = m_treeBkg->Branch("mu_weight", &w_mu_bkg, "mu_weight/D");

  int n_bkg = m_treeBkg->GetEntries();
  for (int i = 0; i < n_bkg; i++) 
  {
     m_treeBkg->GetEntry(i);
     w_mu_bkg = 1;

     b_mu_bkg->Fill();
  }

  // ----------------------------------------------------------------------------
  // fill signal tree with pt weights  
  // ----------------------------------------------------------------------------
  double w_mu_sig;
  TBranch *b_mu_sig = m_treeSig->Branch("mu_weight", &w_mu_sig, "mu_weight/D");

  double mu_sig;
  m_treeSig->SetBranchAddress("mu", &mu_sig);

  int n_sig = m_treeSig->GetEntries();
  for (int i = 0; i < n_sig; i++) 
  { 
     m_treeSig->GetEntry(i);

     int bin = h_w_mu->FindBin(mu_sig);
     w_mu_sig = h_w_mu->GetBinContent(bin);

     b_mu_sig->Fill();
  }

  return StatusCode::SUCCESS;
}

#pragma GCC pop_options
