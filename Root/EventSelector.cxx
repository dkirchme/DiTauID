#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <DiTauID/EventSelector.h>

// EDM includes
#include "xAODEventInfo/EventInfo.h"
#include "xAODTau/DiTauJetContainer.h"

// Root includes
#include <TSystem.h>


// Infrastructure includes:
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "EventLoop/OutputStream.h"
#include "AsgTools/MsgStream.h"
#include "AsgTools/MsgStreamMacros.h"



// this is needed to distribute the algorithm to the workers
ClassImp(EventSelector)

/// helper macro for checking status code
#define EL_RETURN_CHECK( CONTEXT, EXP )             \
  do {                                              \
    if( !EXP.isSuccess() ) {                        \
      Error(CONTEXT,                                \
            XAOD_MESSAGE("Failed to execute: %s"),  \
            #EXP);                                  \
      return EL::StatusCode::FAILURE;               \
    }  \
  } while( false )

#define CHK( CONTEXT, EXP )             \
  do {                                              \
    if( !EXP.isSuccess() ) {                        \
      Error(CONTEXT,                                \
            XAOD_MESSAGE("Failed to execute: %s"),  \
            #EXP);                                  \
    }  \
  } while( false )



// ----------------------------------------------------------------------------
EventSelector::EventSelector ()
{
  m_grlTool = 0;
  m_trigConfigTool = 0;
  m_trigDecisionTool = 0;
  m_name = "EventSelector";
}


// ----------------------------------------------------------------------------
EventSelector::EventSelector (std::string const& name)
{
  m_grlTool = 0;
  m_trigConfigTool = 0;
  m_trigDecisionTool = 0;
  m_name = name;
}


// ----------------------------------------------------------------------------
EventSelector::~EventSelector ()
{
  if (m_grlTool)
  {
    delete m_grlTool;
    m_grlTool = 0;
  }
  if (m_trigConfigTool)
  {
    delete m_trigConfigTool;
    m_trigConfigTool = 0;
  }
  if (m_trigDecisionTool)
  {
    delete m_trigDecisionTool;
    m_trigDecisionTool = 0;
  }
  // if (m_tmt)
  // {
  //   delete m_tmt;
  //   m_tmt = 0;
  // }
  if (m_jetCleaning)
  {
    delete m_jetCleaning;
    m_jetCleaning = 0;
  }
}


// ----------------------------------------------------------------------------
EL::StatusCode EventSelector::setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  job.useXAOD();
  EL_RETURN_CHECK("setupJob()", xAOD::Init());  // call before opening first file

  xAOD::TEvent::EAuxMode mode = xAOD::TEvent::kBranchAccess; // alternative: xAOD::TEvent::kClassAccess;
  xAOD::TEvent event(mode);

  return EL::StatusCode::SUCCESS;
}


// ----------------------------------------------------------------------------
EL::StatusCode EventSelector::initialize ()
{ 
  // Good Runs List
  std::string configdir = getenv("ROOTCOREBIN");
  configdir += "/data/DiTauID/";
  std::string grl = "data15_13TeV.periodAllYear_DetStatus-v75-repro20-01_DQDefects-00-02-02_PHYS_StandardGRL_All_Good.xml";


  m_grlTool = new GoodRunsListSelectionTool(m_name+"_GoodRunsListSelectionTool");
  // const char* GRLFilePath = configdir + grl;
  // const char* fullGRLFilePath = gSystem->ExpandPathName(GRLFilePath);
  const char* fullGRLFilePath = gSystem->ExpandPathName((configdir + grl).c_str());
  std::vector<std::string> vStringGRL;
  vStringGRL.push_back(fullGRLFilePath);

  EL_RETURN_CHECK("initialize()", m_grlTool->setProperty("GoodRunsListVec", vStringGRL));
  EL_RETURN_CHECK("initialize()", m_grlTool->setProperty("PassThrough", false));
  EL_RETURN_CHECK("initialize()", m_grlTool->initialize());

  // Trigger Tools
  m_trigConfigTool = new TrigConf::xAODConfigTool(m_name+"_xAODConfigTool"); // gives us access to the meta-data
  EL_RETURN_CHECK("initialize()", m_trigConfigTool->initialize() );
  ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle( m_trigConfigTool );
  m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
  EL_RETURN_CHECK("initialize()", m_trigDecisionTool->setProperty( "ConfigTool", trigConfigHandle ) ); // connect the TrigDecisionTool to the ConfigTool
  EL_RETURN_CHECK("initialize()", m_trigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision" ) );
  EL_RETURN_CHECK("initialize()", m_trigDecisionTool->initialize() );

  // // Trigger Matching Tool
  // m_tmt = new Trig::MatchingTool(m_name+"_Trig::MatchingTool/DiTauJetMatchingTool");
  // Info("initialize()", "B");
  // m_tmt->msg().setLevel(MSG::WARNING);
  // Info("initialize()", "C");
  // // EL_RETURN_CHECK("initialize()", m_tmt->setProperty("TrigDecisionTool", m_trigDecisionTool ));
  // Info("initialize()", "D");
  // EL_RETURN_CHECK("initialize", m_tmt->initialize());
  // Info("initialize()", "E");

  // Jet Cleaning Tool
  m_jetCleaning = new JetCleaningTool(m_name+"_JetCleaning");
  m_jetCleaning->msg().setLevel(MSG::INFO); 
  EL_RETURN_CHECK("initialize()",
      m_jetCleaning->setProperty("CutLevel", "LooseBad"));
  EL_RETURN_CHECK("initialize()",
      m_jetCleaning->setProperty("DoUgly", false));
  EL_RETURN_CHECK("initialize()",
      m_jetCleaning->initialize());



  return EL::StatusCode::SUCCESS;

}


// ----------------------------------------------------------------------------
bool EventSelector::checkGRL (xAOD::TEvent* event)
{ 
  const xAOD::EventInfo* eventInfo = 0;
  CHK("execute",event->retrieve( eventInfo, "EventInfo"));

  if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) 
    return true;

  // if data, check good run list
  if (m_grlTool->passRunLB(*eventInfo)) 
  {
    return true;
  }

  // event did not pass GRL
  return false; 
}

struct TriggerData
{
  std::string trigger;
  double triggerPt;
  bool fired;
  double prescale;
};

bool lowestPrescale(const TriggerData &a, const TriggerData &b)
{ 
  // prefer the high-pt trigger if both prescales are the same (e.g. 0 or 1)
  if (a.prescale == b.prescale)
    return a.triggerPt > b.triggerPt;

  // prefer prescales that are not zero
  if (a.prescale == 0)
    return false;
  if (b.prescale == 0)
    return true;

  // prefer lower prescales 
  return a.prescale < b.prescale;
}


// ----------------------------------------------------------------------------
bool EventSelector::checkTrigger (xAOD::TEvent* event)
{
  const xAOD::JetContainer* xJetContainer = 0;
  CHK("execute", event->retrieve(xJetContainer, "AntiKt10LCTopoJets"));

  const xAOD::EventInfo* xEventInfo = 0;
  CHK("execute", event->retrieve(xEventInfo, "EventInfo"));

  std::string triggers[] = {"HLT_j100", "HLT_j110", "HLT_j150", "HLT_j175", "HLT_j200", "HLT_j260", "HLT_j300", "HLT_j320", "HLT_j380", "HLT_j400"};
  std::string delimiter = "_j";

  // double prescales[] = {0., 0., 0., 0., 0., 0., 0., 0., 0., 0.};
  // int fired[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

  // for (int i = 0; i < 10; ++i)
  // {
  //   prescales[i] = 0;
  //   fired[i] = 0;
  //   auto cg = m_trigDecisionTool->getChainGroup(triggers[i]);
  //   fired[i] = cg->isPassed();
  //   if (fired[i]) prescales[i] = cg->getPrescale();

  //   xEventInfo->auxdecor<int>(triggers[i].c_str()) = fired[i];
  //   xEventInfo->auxdecor<double>((triggers[i]+"_prescale").c_str()) = prescales[i];
  // }

  // lead jet pt
  if (xJetContainer->size() < 1) return false;
  double leadJetPt = xJetContainer->front()->pt()/1000.;

  std::vector<TriggerData> vtd;
  vtd.clear();

  for (auto trig: triggers)
  {
    TriggerData td;
    td.trigger = trig;
    td.triggerPt = std::stod(trig.substr(trig.find(delimiter)+delimiter.length()));
    td.prescale = 0;
    td.fired = false;

    auto cg = m_trigDecisionTool->getChainGroup(trig);
    td.fired = cg->isPassed();
    if (td.fired) td.prescale = cg->getPrescale();

    vtd.push_back(td);
  }

  std::sort(vtd.begin(), vtd.end(), lowestPrescale);

  for (auto td: vtd)
  {
    // Info("execute()", "Trigger: %s. triggerPt: %.1f. jetPt: %.1f. prescale: %.1f fired: %i.", td.trigger.c_str(), td.triggerPt, leadJetPt, td.prescale, td.fired);
    if (!td.fired)
    {
      // Info("execute", "trigger did not fire.");
      continue;
    }
    if (leadJetPt < td.triggerPt) 
    {
      // Info("execute", "jet pt to low. continue.");
      continue;
    }

    xEventInfo->auxdecor<double>("trigger_pt") = td.triggerPt;
    xEventInfo->auxdecor<double>("trigger_prescale") = td.prescale;

    // Info("execute", "event passed trigger: %s", td.trigger.c_str());
    // Info("execute", "---------------------------------------------");

    return true;
  }
  

  // Info("execute", "could not find trigger. reject event.");
  // Info("execute()", "---------------------------------------------");

  return false;
}


// ----------------------------------------------------------------------------
bool EventSelector::checkJetCleaning(xAOD::TEvent* event)
{
  const xAOD::JetContainer* xJetContainer = 0;
  CHK( "checkJetCleaning()", 
      event->retrieve(xJetContainer, "AntiKt4EMTopoJets") );

  float jet_cut = 60.*1000; // MeV

  for (const auto xJet: *xJetContainer)
  {
    if (xJet->pt() < jet_cut) continue;

    if (!m_jetCleaning->accept(*xJet))
    {
      return false;
    }
  }

  return true;
}


// ----------------------------------------------------------------------------
bool EventSelector::checkEvent (xAOD::TEvent* event)
{
  if (! checkGRL(event))
    return false;
  if (! checkJetCleaning(event))
    return false;
  if (! checkTrigger(event))
    return false;

  return true;
}
