#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <DiTauID/DTNTmaker.h>

// EDM includes
#include "xAODEventInfo/EventInfo.h"
#include "xAODTau/DiTauJetContainer.h"
#include "xAODTruth/TruthParticleContainer.h"


// Root includes
#include <TFile.h>

// Infrastructure includes:
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "EventLoop/OutputStream.h"

typedef std::vector<ElementLink<xAOD::TruthParticleContainer>> TruthParticleLinks_t;

// this is needed to distribute the algorithm to the workers
ClassImp(DTNTmaker)

/// helper macros for checking and retriving xAOD containers
#define CHECKTOOL( ARG )                    \
  do {                                                  \
    const bool result = ARG;                \
    if( ! result ) {                    \
      ::Error( "DTNTmaker", "Failed to execute: \"%s\"",  \
           #ARG );                  \
      return 1;                     \
    }                           \
  } while( false )

#define CHECK( ARG )                    \
  do {                                                  \
    const EL::StatusCode result = ARG;                \
    if( result == EL::StatusCode::FAILURE ) {                    \
      ::Error( "DTNTmaker", "Failed to execute: \"%s\"",  \
           #ARG );                  \
      return 1;                     \
    }                           \
  } while( false )

#define EL_RETURN_CHECK( CONTEXT, EXP )             \
  do {                                              \
    if( !EXP.isSuccess() ) {                        \
      Error(CONTEXT,                                \
            XAOD_MESSAGE("Failed to execute: %s"),  \
            #EXP);                                  \
      return EL::StatusCode::FAILURE;               \
    }  \
  } while( false )






DTNTmaker::DTNTmaker ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode DTNTmaker::setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  job.useXAOD();
  EL_RETURN_CHECK("setupJob()", xAOD::Init());  // call before opening first file

  xAOD::TEvent::EAuxMode mode = xAOD::TEvent::kClassAccess; // alternative: xAOD::TEvent::kBranchAccess;
  xAOD::TEvent event(mode);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode DTNTmaker::histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  // prepare ntuple output
  // the ntuple is intended to be used for DiTauID training and testing
  Info("histInitialize()", "make output file");
  TFile *outputFile = wk()->getOutputFile (outputName);
  Info("histInitialize()", "output file made");
  tree = new TTree ("tree", "tree");
  tree->SetDirectory (outputFile);
  
  tree->Branch("EventNumber", &EventNumber);
  tree->Branch("ev_weight", &ev_weight);
  tree->Branch("sample_weight", &sample_weight);
  tree->Branch("mu", &mu);
  tree->Branch("n_vtx", &n_vtx);
  tree->Branch("jet_pt", &jet_pt);

  tree->Branch("eta", &eta);
  tree->Branch("phi", &phi);
  tree->Branch("m", &m);
  tree->Branch("pt_lead", &pt_lead);
  tree->Branch("pt_subl", &pt_subl);
  tree->Branch("eta_lead", &eta_lead);
  tree->Branch("eta_subl", &eta_subl);
  tree->Branch("phi_lead", &phi_lead);
  tree->Branch("phi_subl", &phi_subl);
  tree->Branch("e_lead", &e_lead);
  tree->Branch("e_subl", &e_subl);

  tree->Branch("ditau_pt", &ditau_pt);
  tree->Branch("isSignal", &isSignal);
  
  tree->Branch("f_core_lead", &f_core_lead);
  tree->Branch("f_core_subl", &f_core_subl);
  
  tree->Branch("f_subjet_lead", &f_subjet_lead);
  tree->Branch("f_subjet_subl", &f_subjet_subl);
  tree->Branch("f_subjets", &f_subjets);
  
  tree->Branch("E_frac_subl", &E_frac_subl);
  tree->Branch("E_frac_subsubl", &E_frac_subsubl);
  
  tree->Branch("f_track_lead", &f_track_lead);
  tree->Branch("f_track_subl", &f_track_subl);
  tree->Branch("f_isotracks", &f_isotracks);
  tree->Branch("f_clusters", &f_clusters);
  
  tree->Branch("R_max_lead", &R_max_lead);
  tree->Branch("R_max_subl", &R_max_subl);
  
  tree->Branch("R_track", &R_track);
  tree->Branch("R_track_all", &R_track_all);
  tree->Branch("R_track_core", &R_track_core);
  tree->Branch("R_isotrack", &R_isotrack);
  
  tree->Branch("R_core_lead", &R_core_lead);
  tree->Branch("R_core_subl", &R_core_subl);
  tree->Branch("R_tracks_lead", &R_tracks_lead);
  tree->Branch("R_tracks_subl", &R_tracks_subl);
  
  tree->Branch("R_subjets_subl", &R_subjets_subl);
  tree->Branch("R_subjets_subsubl", &R_subjets_subsubl);
  
  tree->Branch("m_track", &m_track);
  tree->Branch("m_track_all", &m_track_all);
  tree->Branch("m_track_core", &m_track_core);
  
  tree->Branch("m_core_lead", &m_core_lead);
  tree->Branch("m_core_subl", &m_core_subl);
  tree->Branch("m_tracks_lead", &m_tracks_lead);
  tree->Branch("m_tracks_subl", &m_tracks_subl);

  tree->Branch("d0_leadtrack_lead", &d0_leadtrack_lead);
  tree->Branch("d0_leadtrack_subl", &d0_leadtrack_subl);
  
  tree->Branch("n_track", &n_track);
  tree->Branch("n_tracks_lead", &n_tracks_lead);
  tree->Branch("n_tracks_subl", &n_tracks_subl);
  tree->Branch("n_isotrack", &n_isotrack);
  tree->Branch("n_othertrack", &n_othertrack); 
  tree->Branch("n_iso_ellipse", &n_iso_ellipse);
  tree->Branch("n_Subjets", &n_Subjets);
  
  tree->Branch("n_antikt_subjets", &n_antikt_subjets);
  tree->Branch("n_ca_subjets", &n_ca_subjets);
  tree->Branch("mu_massdrop", &mu_massdrop);
  tree->Branch("y_massdrop", &y_massdrop);
  
  tree->Branch("JetBDT", &JetBDT);

  tree->Branch("trigger_pt", &trigger_pt);
  tree->Branch("trigger_prescale", &trigger_prescale);

  tree->Branch("truth_matched", &truth_matched);
  tree->Branch("truth_pt_vis_lead", &truth_pt_vis_lead);
  tree->Branch("truth_eta_vis_lead", &truth_eta_vis_lead);
  tree->Branch("truth_phi_vis_lead", &truth_phi_vis_lead);
  tree->Branch("truth_E_vis_lead", &truth_E_vis_lead);
  tree->Branch("truth_m_vis_lead", &truth_m_vis_lead);
  tree->Branch("truth_n_tracks_lead", &truth_n_tracks_lead);
  tree->Branch("truth_pt_vis_subl", &truth_pt_vis_subl);
  tree->Branch("truth_eta_vis_subl", &truth_eta_vis_subl);
  tree->Branch("truth_phi_vis_subl", &truth_phi_vis_subl);
  tree->Branch("truth_E_vis_subl", &truth_E_vis_subl);
  tree->Branch("truth_m_vis_subl", &truth_m_vis_subl);
  tree->Branch("truth_n_tracks_subl", &truth_n_tracks_subl);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode DTNTmaker::fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode DTNTmaker::changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.

  xAOD::TEvent* event = wk()->xaodEvent();
  
  const xAOD::EventInfo* eventInfo = 0;
  EL_RETURN_CHECK("changeInput", event->retrieve( eventInfo, "EventInfo"));

  // is simulation?
  isMC = false;
  if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) isMC = true;

  // check if we have a jet background or ditau signal sample
  isSignal = -1;
  if (isMC)
  {
    if (eventInfo->mcChannelNumber() == 303367)  // bbtautau sample M = 2000
      isSignal = 1;
    else if (eventInfo->mcChannelNumber() == 303366)  // bbtautau sample M = 1800
      isSignal = 1;
    else if (eventInfo->mcChannelNumber() == 303368)  // bbtautau sample M = 2250
      isSignal = 1;

    else if (eventInfo->mcChannelNumber() == 361023) // jet sample JZ3W
      isSignal = 0;
    else if (eventInfo->mcChannelNumber() == 361024) // jet sample JZ4W
      isSignal = 0;
    else if (eventInfo->mcChannelNumber() == 361025) // jet sample JZ5W
      isSignal = 0;
    else if (eventInfo->mcChannelNumber() == 361026) // jet sample JZ6W
      isSignal = 0;
    else if (eventInfo->mcChannelNumber() == 361027) // jet sample JZ7W
      isSignal = 0;
    else if (eventInfo->mcChannelNumber() == 410000) // ttbar
      isSignal = 0;
    else
    {
      Warning("changeInput()", "provided dataset with unknown mcChannelNumber");
      isSignal = -1;
    }
  } 

  sample_weight = 1;
  if (isMC)
  {
    if (eventInfo->mcChannelNumber() == 361023 )
    {
      double x_sec = 2.6454E+04;
      double filter_eff = 3.1953E-04;
      int n_events = 1767000;
      sample_weight = (x_sec * filter_eff) / n_events;
    }
    if (eventInfo->mcChannelNumber() == 361024 )
    {
      double x_sec = 2.5464E+02;
      double filter_eff = 5.3009E-04;
      int n_events = 1997000;
      sample_weight = (x_sec * filter_eff) / n_events;
    }
    if (eventInfo->mcChannelNumber() == 361025 )
    {
      double x_sec = 4.5536E+00;
      double filter_eff = 9.2325E-04;
      int n_events = 1995000;
      sample_weight = (x_sec * filter_eff) / n_events;
    }
    if (eventInfo->mcChannelNumber() == 361026 )
    {
      double x_sec = 2.5752E-01;
      double filter_eff = 9.4016E-04;
      int n_events = 1997000;
      sample_weight = (x_sec * filter_eff) / n_events;
    }
    if (eventInfo->mcChannelNumber() == 361027 )
    {
      double x_sec = 1.6214E-02;
      double filter_eff = 3.9282E-04;
      int n_events = 1990000;
      sample_weight = (x_sec * filter_eff) / n_events;
    }
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode DTNTmaker::initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  m_evtSelector = new EventSelector();
  m_evtSelector->initialize();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode DTNTmaker::execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  xAOD::TEvent* event = wk()->xaodEvent();
  const xAOD::EventInfo* eventInfo = 0;
  EL_RETURN_CHECK("execute", event->retrieve( eventInfo, "EventInfo"));

  if (! m_evtSelector->checkEvent(event))
  {
    return EL::StatusCode::SUCCESS;
  }


  // get MC event weight
  ev_weight = 1;
  if (isMC)
  {
    const std::vector<float> weights = eventInfo->mcEventWeights();
    if(weights.size()>0) ev_weight = weights[0];
  }

  // get pile-up (interactions per crossing)
  EventNumber = eventInfo->eventNumber();
  mu = eventInfo->averageInteractionsPerCrossing();

  // get number of primary vertices
  const xAOD::VertexContainer* vxContainer = 0;
  EL_RETURN_CHECK("execute", event->retrieve( vxContainer, "PrimaryVertices"));

  n_vtx = 0;
  for (const auto vtx: *vxContainer)
  {
      if (vtx->nTrackParticles() >= 2) n_vtx++;
  }

  // get trigger infos
  trigger_pt = eventInfo->auxdata< double >("trigger_pt");
  trigger_prescale = eventInfo->auxdata< double >("trigger_prescale");

  // get ditau container
  const xAOD::DiTauJetContainer* xDiTauContainer = 0;
  EL_RETURN_CHECK("execute()", event->retrieve(xDiTauContainer, "DiTauJets"));
  xAOD::DiTauJet xDiTau;

  // loop over ditau candidates
  for (const auto xDiTau: *xDiTauContainer) 
  {
    if (checkCuts(xDiTau))
    {
      // xDiTau = (*xDiTau);
      fillTree(xDiTau);
    }
  }


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode DTNTmaker::postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode DTNTmaker::finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  if (m_evtSelector)
  {
    delete m_evtSelector;
    m_evtSelector = 0;
  }
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode DTNTmaker::histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}


bool DTNTmaker::checkCuts (const xAOD::DiTauJet* xDiTau)
{
  // DiTau signal
  if (isSignal == 1)
  {
    if (xDiTau->nSubjets() < 2)
      return false;
    if (fabs(xDiTau->eta()) > 2.5)
      return false;   
    if (xDiTau->auxdata<double>("ditau_pt") < 200*1000)
      return false;
    if (!xDiTau->auxdata<char>("IsTruthMatched"))
      return false;
  }

  // QCD jet background
  if (isSignal == 0)
  {
    if (xDiTau->nSubjets() < 2)
      return false;
    if (fabs(xDiTau->eta()) > 2.5)
      return false;
    if (xDiTau->auxdata<double>("ditau_pt") < 200*1000)
      return false;
  }

  // data
  if (isSignal == -1)
  {
    if (xDiTau->nSubjets() < 2)
      return false;
    if (fabs(xDiTau->eta()) > 2.5)
      return false;
    if (xDiTau->auxdata<double>("ditau_pt") < 200*1000)
      return false;
  }

  // pass true if cuts are fulfilled
  return true;
}

void DTNTmaker::fillTree (const xAOD::DiTauJet* xDiTau)
{
  // fill all variables 
  jet_pt = xDiTau->pt();

  eta = xDiTau->eta();
  phi = xDiTau->phi();
  m = xDiTau->m();
  pt_lead = xDiTau->subjetPt(0);
  pt_subl = xDiTau->subjetPt(1);
  eta_lead = xDiTau->subjetEta(0);
  eta_subl = xDiTau->subjetEta(1);
  phi_lead = xDiTau->subjetPhi(0);
  phi_subl = xDiTau->subjetPhi(1);
  e_lead = xDiTau->subjetE(0);
  e_subl = xDiTau->subjetE(1);

  ditau_pt = xDiTau->auxdata< double >("ditau_pt");
  
  f_core_lead = xDiTau->auxdata< double >("f_core_lead");
  f_core_subl = xDiTau->auxdata< double >("f_core_subl");
  
  f_subjet_lead = xDiTau->auxdata< double >("f_subjet_lead");
  f_subjet_subl = xDiTau->auxdata< double >("f_subjet_subl");
  f_subjets = xDiTau->auxdata< double >("f_subjets");
  
  E_frac_subl = xDiTau->auxdata< double >("E_frac_subl");
  E_frac_subsubl = xDiTau->auxdata< double >("E_frac_subsubl");
  
  f_track_lead = xDiTau->auxdata< double >("f_track_lead");
  f_track_subl = xDiTau->auxdata< double >("f_track_subl");
  f_isotracks = xDiTau->auxdata< double >("f_isotracks");
  f_clusters = xDiTau->auxdata< double >("f_clusters");
  
  R_max_lead = xDiTau->auxdata< double >("R_max_lead");
  R_max_subl = xDiTau->auxdata< double >("R_max_subl");
  
  R_track = xDiTau->auxdata< double >("R_track");
  R_track_all = xDiTau->auxdata< double >("R_track_all");
  R_track_core = xDiTau->auxdata< double >("R_track_core");
  R_isotrack = xDiTau->auxdata< double >("R_isotrack");
  
  R_core_lead = xDiTau->auxdata< double >("R_core_lead");
  R_core_subl = xDiTau->auxdata< double >("R_core_subl");
  R_tracks_lead = xDiTau->auxdata< double >("R_tracks_lead");
  R_tracks_subl = xDiTau->auxdata< double >("R_tracks_subl");
  
  R_subjets_subl = xDiTau->auxdata< double >("R_subjets_subl");
  R_subjets_subsubl = xDiTau->auxdata< double >("R_subjets_subsubl");
  
  m_track = xDiTau->auxdata< double >("m_track");
  m_track_all = xDiTau->auxdata< double >("m_track_all");
  m_track_core = xDiTau->auxdata< double >("m_track_core");
  
  m_core_lead = xDiTau->auxdata< double >("m_core_lead");
  m_core_subl = xDiTau->auxdata< double >("m_core_subl");
  m_tracks_lead = xDiTau->auxdata< double >("m_tracks_lead");
  m_tracks_subl = xDiTau->auxdata< double >("m_tracks_subl");
  
  d0_leadtrack_lead = xDiTau->auxdata< double >("d0_leadtrack_lead");
  d0_leadtrack_subl = xDiTau->auxdata< double >("d0_leadtrack_subl");
  
  n_track = xDiTau->auxdata< int >("n_track");
  n_tracks_lead = xDiTau->auxdata< int >("n_tracks_lead");
  // n_tracks_lead = xDiTau->auxdata<std::vector<int>>("n_tracks").at(0);
  n_tracks_subl = xDiTau->auxdata< int >("n_tracks_subl");
  // n_tracks_subl = xDiTau->auxdata<std::vector<int>>("n_tracks").at(1);
  n_isotrack = xDiTau->auxdata< int >("n_isotrack");
  n_othertrack = xDiTau->auxdata< int >("n_othertrack");
  n_iso_ellipse = xDiTau->auxdata< int >("n_iso_ellipse");
  n_Subjets = xDiTau->nSubjets();

  n_antikt_subjets = xDiTau->auxdata < int >("n_antikt_subjets");
  n_ca_subjets = xDiTau->auxdata < int >("n_ca_subjets");
  mu_massdrop = xDiTau->auxdata < double >("mu_massdrop");
  y_massdrop = xDiTau->auxdata < double >("y_massdrop");

  JetBDT = xDiTau->auxdata < double >("JetBDT");

  if (isMC)
  {
    truth_matched = xDiTau->auxdata<char>("IsTruthMatched");
  }
  else
  {
    truth_matched = false;
  }

  if ((bool)truth_matched)
  {
    TruthParticleLinks_t truthTaus = xDiTau->auxdata<TruthParticleLinks_t>("TruthTaus");
    const xAOD::TruthParticle* truthTauLead = *truthTaus.at(0);
    const xAOD::TruthParticle* truthTauSubl = *truthTaus.at(1);
    TLorentzVector tlv = TLorentzVector();
    
    truth_pt_vis_lead = truthTauLead->auxdata<double>("pt_vis");
    truth_eta_vis_lead = truthTauLead->auxdata<double>("eta_vis");
    truth_phi_vis_lead = truthTauLead->auxdata<double>("phi_vis");
    truth_m_vis_lead = truthTauLead->auxdata<double>("m_vis");
    tlv.SetPtEtaPhiM(truth_pt_vis_lead, truth_eta_vis_lead, truth_phi_vis_lead, truth_m_vis_lead);
    truth_E_vis_lead = tlv.E();
    truth_n_tracks_lead = truthTauLead->auxdata<size_t>("numCharged");

    truth_pt_vis_subl = truthTauSubl->auxdata<double>("pt_vis");
    truth_eta_vis_subl = truthTauSubl->auxdata<double>("eta_vis");
    truth_phi_vis_subl = truthTauSubl->auxdata<double>("phi_vis");
    truth_m_vis_subl = truthTauSubl->auxdata<double>("m_vis");
    tlv.SetPtEtaPhiM(truth_pt_vis_subl, truth_eta_vis_subl, truth_phi_vis_subl, truth_m_vis_subl);
    truth_E_vis_subl = tlv.E();
    truth_n_tracks_subl = truthTauSubl->auxdata<size_t>("numCharged");
  }
  else
  {
    truth_pt_vis_lead = 0.;
    truth_eta_vis_lead = 0.;
    truth_phi_vis_lead = 0.;
    truth_E_vis_lead = 0.;
    truth_m_vis_lead = 0.;
    truth_n_tracks_lead = 0.;
    truth_pt_vis_subl = 0.;
    truth_eta_vis_subl = 0.;
    truth_phi_vis_subl = 0.;
    truth_E_vis_subl = 0.;
    truth_m_vis_subl = 0.;
    truth_n_tracks_subl = 0.;    
  }

  // fill the tree
  tree->Fill();

}

