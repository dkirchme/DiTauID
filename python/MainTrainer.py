################################################################################
# * Main macro to execute DiTauID training using TMVA
# * handles yaml configuration files
# * parses command line arguments
# * depends on PlottingMacros
################################################################################

# python includes
import os
import argparse

import ROOT
import multiprocessing

# macro includes
from MacroLogger import GetLogger
from MacroParser import GetLoggerParser
from EmbeddedIPython import breakpoint as bp
from ConfigHandler import ConfigHandler
from Silence import Silence
from TMVATraining import TMVATraining




# ----------------------------------------------------------------------------
def main(): 
    # setup argparse
    logger_parser = GetLoggerParser(default_level="INFO")
    desc = "Macro to plot validation variables for DiTau Reconstruction."

    aP = argparse.ArgumentParser(description=desc, parents=[logger_parser],
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    aP.add_argument('-c', '--config',
                    type=str,
                    default='config/training-custom.yaml',
                    help='Path to custom yaml config file.')

    aP.add_argument('-d', '--default_config',
                    type=str,
                    default='config/training-default.yaml',
                    help='Path to default yaml config file.')

    aP.add_argument('-o', '--output_path',
                    type=str,
                    default='./output/',
                    help='Output path.')

    aP.add_argument('-n', '--n_cores', 
                    type=int,
                    default=2,
                    help='set number of processes for multiprocessing')

    args = aP.parse_args()

    global logger
    logger = GetLogger("TMVATraining", args.loggerlevel)
    logger.debug(vars(args))


    config = ConfigHandler(name='ConfigHandler', logLevel=args.loggerlevel)
    config.SetConfigsByYaml(custom=args.config, default=args.default_config)

    # jobs = []
    # for (training, options) in config.GetConfigs():
    #     p = multiprocessing.Process(name=training,
    #                                 target=tmva_training,
    #                                 args=(args, options))
    #     jobs.append(p)
    #     p.start()

    global n_jobs, job_count
    n_jobs = config.GetNConfigs()
    job_count = 0

    logger.info("number of trainings to be performed: {}".format(n_jobs))

    pool = multiprocessing.Pool(args.n_cores)
    for (training, options) in config.GetConfigs():
        pool.apply_async(tmva_training,
                         args=(training, args, options),
                         callback=finish)
                         # args=(training, args, options))

    pool.close()
    pool.join()

    return

# ----------------------------------------------------------------------------
def tmva_training(name, args, options):
    multiprocessing.current_process().name = name

    output_path = check_output_path(args.output_path)
    if not output_path:
        return

    global logger
    if ("-" in name):
        logger.warning("Found unrecommended '-' in "+name+". Replace with '_'.")
        name = name.replace("-", "_")
        
    logger.info("start training: {}".format(name))

    out_file = "{}{}.log".format(output_path, name)

    with Silence(stdout=out_file, mode='w'):
        tmva = TMVATraining(name=name, logLevel=args.loggerlevel)
        tmva.SetOptions(**options)
        tmva.SetOutputPath(output_path)
        tmva.Train()

    logger.info("finish training: {}".format(name))
    return


# ----------------------------------------------------------------------------
def finish(arg):
    global logger, n_jobs, job_count
    job_count += 1

    logger.info("Trainings left: {}/{}".format(n_jobs-job_count, n_jobs))

    return

    
# ----------------------------------------------------------------------------
def check_output_path(path):
    global logger

    try:
        if path[-1] is not '/': path += '/'    

        if os.path.isdir(path) is False:
            logger.error('{} is not a directory. Can not write log file'.format(path))
            return None
    except Exception as e:
        logger.error(e)
        raise

    return path


# ----------------------------------------------------------------------------
if __name__ == "__main__":
    main()


