################################################################################
# * Class to perform TMVA training
# * can be configured in config/training-custom.yaml
################################################################################

import os

import ROOT

# common includes
from MacroLogger import BaseLoggerClass
from OptionHandler import BaseOptionClass
from EmbeddedIPython import breakpoint as bp

import json


################################################################################
class TMVATraining(BaseLoggerClass, BaseOptionClass):
    """docstring for TMVATraining"""
    def __init__(self, name, logLevel="WARNING"):
        BaseLoggerClass.__init__(self, name, logLevel)
        BaseOptionClass.__init__(self, name, logLevel)

        self.name = name
        self.cwd = os.getcwd() # current working directory
        self.out_path = self.cwd  # path to store weights and root files

    #######################################
    def SetOutputPath(self, path):
        if os.path.isdir(path) is False:
            self.logger.warning('{} is not a directory.'.format(path))
            return

        self.out_path = os.path.abspath(path)
        self.logger.debug('set tmva output path: {}'.format(self.out_path))
    
    #######################################
    def Train(self):
        opt = self.Options()

        os.chdir(self.out_path)

        f_sig = ROOT.TFile(opt['sigFile'], "READ")
        f_bkg = ROOT.TFile(opt['bkgFile'], "READ")

        tree_sig = f_sig.Get(opt['treeName']);
        tree_bkg = f_bkg.Get(opt['treeName']);



        # ----------------------------------------------------------------------------
        # defining training methods and variables 
        # ----------------------------------------------------------------------------
        ROOT.TMVA.Tools.Instance()
         
        # f_out = ROOT.TFile(opt['outputFile'], "RECREATE")
        f_out = ROOT.TFile("{}.root".format(self.name), "RECREATE")
         
        factory = ROOT.TMVA.Factory("TMVAClassification", 
                               f_out,
                               ":".join([ 
                                          "!V",
                                          "!Silent",
                                          "Color",
                                          "DrawProgressBar",
                                          "Transformations="+opt["Transformations"],
                                          "AnalysisType=Classification" ]))

        for (var, vartype) in opt['variables']:
            factory.AddVariable(var, vartype)


        factory.AddSignalTree(tree_sig, 1)
        factory.AddBackgroundTree(tree_bkg, 1)

        for (var, vartype) in opt['spectators']:
            factory.AddSpectator(var, var, vartype)

        factory.SetWeightExpression(opt['weight'])
         
        # cuts defining the signal and background sample
        cut_sig = ROOT.TCut(opt['sigCut'])
        cut_bkg = ROOT.TCut(opt['bkgCut'])
         
        factory.PrepareTrainingAndTestTree(cut_sig, cut_bkg,
                                           ":".join([
                                                # "nTrain_Signal=0", # default all
                                                # "nTest_Signal=0", # default all (here splitting half/half)
                                                # "nTrain_Background=0", 
                                                # "nTest_Background=0", 
                                                "SplitMode={}".format(opt['SplitMode']),
                                                "SplitSeed={}".format(opt['SplitSeed']), # different random series every time
                                                "NormMode={}".format(opt['NormMode']),
                                                "!V" ]))

        # ----------------------------------------------------------------------------
        # training and creation of weight files for all methods defined above
        # ----------------------------------------------------------------------------
        for mopt in opt['methods']:
            method = factory.BookMethod(ROOT.TMVA.Types.kBDT, mopt['bdtName'],
                         ":".join([
                              "!H",
                              "!V",
                              "NTrees={}".format(mopt['NTrees']),
                              # "nEventsMin=150",
                              "MaxDepth={}".format(mopt['MaxDepth']),
                              "BoostType=AdaBoost",
                              "AdaBoostBeta={}".format(mopt['AdaBoostBeta']),
                              "UseYesNoLeaf=FALSE", # was ist das?
                              "SeparationType=GiniIndex",
                              "nCuts={}".format(mopt['nCuts']),
                              "PruneMethod=NoPruning",
                              "DoBoostMonitor:MinNodeSize={}".format(mopt['MinNodeSize']) # hoeher!
                              ]))

        factory.TrainAllMethods()
        factory.TestAllMethods()
        factory.EvaluateAllMethods()

        config = ROOT.TNamed("config_json", json.dumps(opt))
        config.Write()

        f_sig.Close()
        f_bkg.Close()
        f_out.Close()

        os.chdir(self.cwd)
