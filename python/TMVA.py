import ROOT

variables = []
variables.append(('f_core_subl', 'F'))
variables.append(('f_core_lead', 'F'))
variables.append(('R_max_lead', 'F'))
variables.append(('R_max_subl', 'F'))
variables.append(('n_tracks_lead', 'I'))
variables.append(('n_tracks_subl', 'I'))


# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
if __name__ == "__main__":

    # f_in = TFile("DitauNTuple_split.root", "READ")
    f_sig = ROOT.TFile("~/DiTauRecValidation/Run/run04/data-DTNT/DTNT_Signal.root", "READ")
    f_bkg = ROOT.TFile("~/DiTauRecValidation/Run/run04/data-DTNT/DTNT_Background.root", "READ")

    # ntuple_train = f_in.Get("training")
    # ntuple_test = f_in.Get("testing")
    tree_sig = f_sig.Get("tree");
    tree_bkg = f_bkg.Get("tree");

    # ----------------------------------------------------------------------------
    # defining training methods and variables 
    # ----------------------------------------------------------------------------
    ROOT.TMVA.Tools.Instance()
     
    f_out = ROOT.TFile("TMVA.root", "RECREATE")
     
    factory = ROOT.TMVA.Factory("TMVAClassification", 
                           f_out,
                           ":".join([ 
                                      "!V",
                                      "!Silent",
                                      "Color",
                                      "DrawProgressBar",
                                      "Transformations=I;D;P;G,D",
                                      "AnalysisType=Classification" ]))

    for (v,k) in variables:
        factory.AddVariable(v, k)


    factory.AddSignalTree(tree_sig, 1)
    factory.AddBackgroundTree(tree_bkg, 1)

    factory.AddSpectator('ditau_pt', 'ditau pt', 'F')
    factory.AddSpectator('mu', 'mu', 'I')
    factory.AddSpectator('isSignal', 'isSignal', 'I')

    factory.SetWeightExpression("pt_weight")
     
    # cuts defining the signal and background sample
    cut_sig = ROOT.TCut("isSignal > 0.5")
    cut_bkg = ROOT.TCut("isSignal <= 0.5")
     
    factory.PrepareTrainingAndTestTree(cut_sig, cut_bkg,
                                       ":".join([
                                            # "nTrain_Signal=0", # default all
                                            # "nTest_Signal=0", # default all (here splitting half/half)
                                            # "nTrain_Background=0", 
                                            # "nTest_Background=0", 
                                            "SplitMode=Random",
                                            "SplitSeed=0", # different random series every time
                                            "NormMode=EqualNumEvents",
                                            "!V" ]))

    # ----------------------------------------------------------------------------
    # training and creation of weight files for all methods defined above
    # ----------------------------------------------------------------------------
    method = factory.BookMethod(ROOT.TMVA.Types.kBDT, "BDT1",
                                         ":".join([
                                              "!H",
                                              "!V",
                                              "NTrees=50",
                                              # "nEventsMin=150",
                                              "MaxDepth=8",
                                              "BoostType=AdaBoost",
                                              "AdaBoostBeta=0.2",
                                              "UseYesNoLeaf=FALSE", # was ist das?
                                              "SeparationType=GiniIndex",
                                              "nCuts=100",
                                              "PruneMethod=NoPruning",
                                              "DoBoostMonitor:MinNodeSize=1" # hoeher!
                                              ]))
     
    factory.TrainAllMethods()
    factory.TestAllMethods()
    factory.EvaluateAllMethods()

    f_sig.Close()
    f_bkg.Close()
    f_out.Close()

        
    # ----------------------------------------------------------------------------
    # plotting
    # ----------------------------------------------------------------------------

    f   = ROOT.TFile("TMVA.root", "READ")

    # vBDT = ["BDT1", "BDT2", "BDT3"]
    vBDT = ["BDT1"]
    vCol = [ROOT.kBlack, ROOT.kRed, ROOT.kBlue]

    vh_score_S = []
    vh_score_B = []
    vh_roc1 = []
    vh_roc2 = []

    for BDT in vBDT:
        vh_score_S.append(f.Get("Method_BDT/{}/MVA_{}_S".format(BDT, BDT)))
        vh_score_B.append(f.Get("Method_BDT/{}/MVA_{}_B".format(BDT, BDT)))
        vh_roc1.append(f.Get("Method_BDT/{}/MVA_{}_rejBvsS".format(BDT, BDT)))
        vh_roc2.append(f.Get("Method_BDT/{}/MVA_{}_invBeffvsSeff".format(BDT, BDT)))


    c = []
    for i in range(len(vBDT)):
        c.append(ROOT.TCanvas(vBDT[i], vBDT[i], 800, 600))
        col = vCol[i]
        vh_score_B[i].SetMarkerColor(col)
        vh_score_S[i].SetMarkerColor(col)
        vh_score_B[i].SetLineColor(col)
        vh_score_S[i].SetLineColor(col)
        vh_score_B[i].Draw("SAME")
        vh_score_S[i].Draw("SAME")

    c.append(ROOT.TCanvas("ROC1", "ROC1", 800, 600))
    vh_roc1[0].Draw()
    for i in range(len(vh_roc1)):
        col  = vCol[i]
        vh_roc1[i].SetLineColor(col)
        vh_roc1[i].Draw("SAME") 

    c.append(ROOT.TCanvas("ROC2", "ROC2", 800, 600))
    vh_roc2[0].Draw()
    for i in range(len(vh_roc2)):
        col  = vCol[i]
        vh_roc2[i].SetLineColor(col)
        vh_roc2[i].Draw("SAME")

