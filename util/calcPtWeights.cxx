// ===========================================================================
// Pt weight calculation
// ===========================================================================
#include <TSystem.h>

#include "DiTauID/WeightsCalculator.h"

#define CHECK( ARG )                    \
  do {                                                  \
    const bool result = ARG;                \
    if( ! result ) {                    \
      ::Error( "calcDiTauVariables", "Failed to execute: \"%s\"",  \
           #ARG );                  \
      return 1;                     \
    }                           \
  } while( false )




int main( int argc, char* argv[] ) {

  DiTauID::WeightsCalculator* WeightsCalculator = new DiTauID::WeightsCalculator("WeightsCalculator");
  WeightsCalculator->msg().setLevel( MSG::INFO );

  // std::string bkgFolder = "/ZIH.fast/users/kirchmeier/DTNT/data_v10";
  // std::string sigFolder = "/ZIH.fast/users/kirchmeier/DTNT/signal_v1";
  std::string bkgFolder = "/ZIH.fast/users/kirchmeier/DTNT/data_v14";
  std::string sigFolder = "/ZIH.fast/users/kirchmeier/DTNT/signal_v2";

  // std::string bkgFolder = "/ZIH.fast/users/kirchmeier/testDTNT/data";
  // std::string sigFolder = "/ZIH.fast/users/kirchmeier/testDTNT/signal";

  // std::string bkgOutFile = bkgFolder+"/DTNT_Background.root";
  // std::string sigOutFile = sigFolder+"/DTNT_Signal.root";
  std::string bkgOutFile = bkgFolder+"/DTNT_Background_flatpt.root";
  std::string sigOutFile = sigFolder+"/DTNT_Signal_flatpt.root";

  CHECK(WeightsCalculator->setProperty("InputFolderSignal", sigFolder));
  CHECK(WeightsCalculator->setProperty("InputFolderBackground", bkgFolder));

  CHECK(WeightsCalculator->setProperty("InputFilePatternSignal", "user.dkirchme.*.RS_G_hh_bbtt*.root*"));
  CHECK(WeightsCalculator->setProperty("InputFilePatternBackground", "user.dkirchme.*DTNT*.root*"));

  CHECK(WeightsCalculator->setProperty("TreeNameSignal", "tree"));
  CHECK(WeightsCalculator->setProperty("TreeNameBackground", "tree"));

  CHECK(WeightsCalculator->setProperty("OutputFileSignal", sigOutFile));
  CHECK(WeightsCalculator->setProperty("OutputFileBackground", bkgOutFile));

  CHECK(WeightsCalculator->setProperty("FlatPtWeight", true));

  CHECK(WeightsCalculator->initialize());
  CHECK(WeightsCalculator->calculateWeights());

  delete WeightsCalculator;

  std::cout << "write " << sigOutFile << std::endl;
  std::cout << "write " << bkgOutFile << std::endl;

  std::cout << "job finished." << std::endl;
}
